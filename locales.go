package locales

type Language string

const (
	LanguageRussian     Language = "ru"
	LanguageKazakh      Language = "kk"
	LanguageBelarusian  Language = "be"
	LanguageGeorgian    Language = "ka"
	LanguageArmenian    Language = "hy"
	LanguageAzerbaijani Language = "az"
	LanguageTurkish     Language = "tr"
	LanguageArabic      Language = "ar"
)

type Declinations[T any] map[Language]*T
