package locales

import (
	"strings"
)

var letters = map[string]string{
	" ":  "-",
	"*":  "",
	"&":  "",
	",":  "",
	"(":  "",
	")":  "",
	":":  "",
	"\"": "",
	"'":  "",

	"ў": "u",
	"ä": "a",
	"ğ": "g",
	"ı": "i",
	"ö": "o",
	"ū": "u",
	"ü": "u",
	"ş": "s",
	"ñ": "n",

	"а": "a",
	"б": "b",
	"в": "v",
	"г": "g",
	"д": "d",
	"е": "e",
	"ё": "yo",
	"ж": "zh",
	"з": "z",
	"и": "i",
	"й": "j",
	"к": "k",
	"л": "l",
	"м": "m",
	"н": "n",
	"о": "o",
	"п": "p",
	"р": "r",
	"с": "s",
	"т": "t",
	"у": "u",
	"ф": "f",
	"х": "h",
	"ц": "c",
	"ч": "ch",
	"ш": "sh",
	"щ": "shch",
	"ъ": "",
	"ы": "y",
	"ь": "",
	"э": "e",
	"ю": "yu",
	"я": "ya",
}

func replace(cyrillic string, dictionary map[string]string) string {

	var replacement strings.Builder
	runes := []rune(cyrillic)

	for _, char := range runes {

		if lat, ok := dictionary[string(char)]; ok {
			replacement.WriteString(lat)
			continue
		}

		replacement.WriteRune(char)
	}

	return replacement.String()
}

func Transliterate(cyrillic string) string {
	return replace(strings.ToLower(cyrillic), letters)
}
